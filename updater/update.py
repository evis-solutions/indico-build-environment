import gitlab
import os
import re
import requests
from packaging import version
import pathlib

#
# Get Indico releases from Github
#
github_releases = requests.get("https://api.github.com/repos/indico/indico/releases")
release_dict = {}

#
# Build a release dictionary with the latest release per series
#
for release in github_releases.json():
    if release["draft"] or release["prerelease"]:
        continue

    git_version = version.parse(release["tag_name"])
    if not version:
        continue

    series = str(git_version.major) + "." + str(git_version.minor)
    if not series in release_dict:
        release_dict[series] = {"version": git_version}

    if git_version >= release_dict[series]["version"]:
        release_dict[series]["url"]  = release["html_url"]
        release_dict[series]["body"] = release["body"]
        release_dict[series]["name"] = release["name"]
        release_dict[series]["tag"]  = release["tag_name"]

#
# Get build configuration from existing folders in repository
#
dir = pathlib.Path(__file__).parent.resolve().parent
builds = [name for name in os.listdir(dir) if os.path.isdir(os.path.join(dir, name)) and re.match(r'\d+\.\d+', name)]

#
# Create Gitlab API client
#
gl = gitlab.Gitlab("https://gitlab.com", private_token=os.environ["GITLAB_ACCESS_TOKEN"])
try:
    project = gl.projects.get(os.environ["CI_PROJECT_PATH"])
except gitlab.exceptions.GitlabGetError:
    print("Error: Project %s not found on Gitlab." % os.environ["CI_PROJECT_PATH"])
    quit()

#
# Check for all build configurations if an update is available
#
for series in sorted(builds):
    print("Checking updates for %s directory..." % series)

    dockerfile = pathlib.Path(os.path.join(dir, series, "Dockerfile")).read_text()
    m = re.search(r'\(release (\d+\.\d+\.?\d*)\)', dockerfile)
    if not m:
        print("Invalid Dockerfile. Skipped.")
        continue

    installed_version = version.parse(m.expand(r'\1'))
    if not series in release_dict:
        print("Installed: %s, no release information available." % installed_version)
        continue

    latest_version = release_dict[series]["version"]
    if installed_version >= latest_version:
        print("Installed: %s, latest: %s. Nothing to do." % (installed_version, latest_version))
        continue

    print("Installed: %s, latest: %s. Checking update..." % (installed_version, latest_version))
    branch = "indico-" + latest_version.base_version
    try:
        branch = project.branches.get(branch)
        print("Update already pushed to Gitlab.")
        continue
    except gitlab.exceptions.GitlabGetError:
        pass

    print("Creating commit with updated files...")
    dockerfile = re.sub(r'\(release (\d+\.\d+\.?\d*)\)', "(release " + latest_version.base_version + ")", dockerfile)
    dockerfile = re.sub(r'indico/indico/v?\d+\.\d+.?\d*/', "indico/indico/" + release_dict[series]["tag"] + "/", dockerfile)
    readme = pathlib.Path(os.path.join(dir, "README.md")).read_text()
    readme = re.sub(series.replace(".", "\\.") + r"\.\d+ release", latest_version.base_version + " release", readme)
    data = {
        "branch": branch,
        "start_branch": "master",
        "commit_message": "Updated Indico %s to %s" % (installed_version, latest_version),
        "actions": [
            {
                "action": "update",
                "file_path": os.path.join(series, "Dockerfile"),
                "content": dockerfile
            },
            {
                "action": "update",
                "file_path": "README.md",
                "content": readme
            }
        ]
    }
    commit = project.commits.create(data)
    
    print("Creating merge request...")
    project.mergerequests.create({
        "source_branch": branch,
        "target_branch": "master",
        "title": "Update Indico %s series from %s to %s" % (series, installed_version, latest_version),
        "description": "# Indico %s\n\n" % release_dict[series]["name"]
            + "Update from Indico %s to %s<br>\n" % (installed_version, latest_version)
            + "Release Notes: %s\n\n" % release_dict[series]["url"]
            + release_dict[series]["body"],
        "remove_source_branch": True
    })

print("Done.")
