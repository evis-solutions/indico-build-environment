# 
# This Docker image contains all necessary build tools for Indico (release 2.2.8)
#
FROM python:2.7-stretch

# Install Indico build dependencies
RUN apt-get update \
    && apt-get install -y --install-recommends python-dev python-virtualenv libxslt1-dev \
        libxml2-dev libffi-dev libpcre3-dev libyaml-dev build-essential redis-server \
        libjpeg62-turbo-dev default-jre libssl-dev postgresql-client openssh-client \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install -y nodejs \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Pre-Install Indico dependencies so that they are cached
RUN pip install --upgrade pip \
    && pip install 'validators==0.14.2' \
    && wget -q https://raw.githubusercontent.com/indico/indico/v2.2.8/requirements.txt \
    && pip install --cache-dir /cache/pip -r requirements.txt \
    && wget -q https://raw.githubusercontent.com/indico/indico/v2.2.8/requirements.dev.txt \
    && pip install --cache-dir /cache/pip -r requirements.dev.txt

# Add python command
CMD ["python"]
