# Indico build environment

This repository contains several Docker images with all prerequisites to build your own Indico image.

## Different versions

Different Docker images are offered for each major Indico version. These images mainly differ in the Python packages which have been pre-installed (i.e., cached). This can drastically reduce build time. The following images are available:

| Image  | Indico          | Status          |
|--------|-----------------|-----------------|
| latest | master branch   | ✅              |
| 3.3    | 3.3.4 release   | ✅              |
| 3.2    | 3.2.9 release   | ✅              |
| 3.1    | 3.1.1 release   | ❌ discontinued |
| 3.0    | 3.0.3 release   | ❌ discontinued |
| 2.3    | 2.3.5 release   | ❌ discontinued |
| 2.2    | 2.2.8 release   | ❌ discontinued |
| 2.1    | 2.1.11 release  | ❌ discontinued |
| 2.0    | 2.0.3 release   | ❌ discontinued |

## Using this image

The above-metioned images are available from [Dockerhub](https://hub.docker.com/r/evissolutions/indico-build-environment) or from [Gitlab's Container Registry](https://gitlab.com/evis-solutions/indico-build-environment/container_registry). Since the images are identical, it does not matter which source you are using.

To pull the images from Dockerhub:

```
docker pull evissolutions/indico-build-environment
```

To pull the images from Gitlab:

```
docker pull registry.gitlab.com/evis-solutions/indico-build-environment
```

## Build Indico wheel

You can now start building your own Indico wheel by running the following commands inside the Docker image:

```
pip install -r requirements.dev.txt
pip install -e .
npm ci --prefer-offline
mkdir ./dist/
./bin/maintenance/build-wheel.py indico --add-version-suffix --ignore-unclean
```

## Build wheel for Indico plugin

To build a wheel for an Indico plugin, you need to make sure that you have the `requirements.txt` and `build-wheel.py` from the main Indico repository available with the code of your plugins. Then, you can run the following commands inside the Docker image:

```
pip install -r ./indico/requirements.txt
mkdir ./dist/
./indico/bin/maintenance/build-wheel.py -d ./dist/ plugin ./payment_manual/ --add-version-suffix --ignore-unclean
./indico/bin/maintenance/build-wheel.py -d ./dist/ plugin ./payment_paypal/ --add-version-suffix --ignore-unclean
```

## Continous Integration with GitLab

Using GitLab Runners and this Docker image, continous integration for Indico is just one step away. The following sample configuration in `.gitlab-ci.yml` enables running Indico's unit tests and building of custom Indico wheels. For further information please refer to the [GitLab Runner documentation](https://docs.gitlab.com/runner/).

```
image: evissolutions/indico-build-environment:latest

variables:
  POSTGRES_DB: indico
  POSTGRES_USER: indico
  POSTGRES_PASSWORD: indico

stages:
  - test
  - build

test-python:
  stage: test
  services:
    - postgres
  script:
    - pip install --cache-dir /cache/pip -r requirements.dev.txt
    - pip install --cache-dir /cache/pip -e .
    - export PGPASSWORD=$POSTGRES_PASSWORD
    - psql -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "CREATE EXTENSION unaccent;"
    - psql -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "CREATE EXTENSION pg_trgm;"
    - export INDICO_TEST_DATABASE_URI="postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@postgres/$POSTGRES_DB"
    - python bin/maintenance/update_backrefs.py --ci
    - pytest

build-wheels:
  stage: build
  script:
    - pip install --cache-dir /cache/pip -r requirements.dev.txt
    - pip install --cache-dir /cache/pip -e .
    - npm config set --global cache .npm/
    - npm ci --prefer-offline
    - mkdir ./dist/
    - ./bin/maintenance/build-assets.py indico --clean
    - ./bin/maintenance/build-wheel.py indico --no-assets --add-version-suffix --ignore-unclean
```
